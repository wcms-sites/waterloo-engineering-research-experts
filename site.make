core = 7.x
api = 2

; behavior_weights
projects[behavior_weights][type] = "module"
projects[behavior_weights][download][type] = "git"
projects[behavior_weights][download][url] = "https://git.uwaterloo.ca/drupal-org/behavior_weights.git"
projects[behavior_weights][download][tag] = "7.x-1.0"
projects[behavior_weights][subdir] = ""

; blockreference
projects[blockreference][type] = "module"
projects[blockreference][download][type] = "git"
projects[blockreference][download][url] = "https://git.uwaterloo.ca/drupal-org/blockreference.git"
projects[blockreference][download][tag] = "7.x-2.4"
projects[blockreference][subdir] = ""

; salesforce
projects[salesforce][type] = "module"
projects[salesforce][download][type] = "git"
projects[salesforce][download][url] = "https://git.uwaterloo.ca/drupal-org/salesforce.git"
projects[salesforce][download][tag] = "7.x-3.2"
projects[salesforce][subdir] = ""

; salesforce_webforms
projects[salesforce_webforms][type] = "module"
projects[salesforce_webforms][download][type] = "git"
projects[salesforce_webforms][download][url] = "https://git.uwaterloo.ca/drupal-org/salesforce_webforms.git"
projects[salesforce_webforms][download][tag] = "7.x-1.3"
projects[salesforce_webforms][subdir] = ""

; uw_ofis_eng
projects[uw_ofis_eng][type] = "module"
projects[uw_ofis_eng][download][type] = "git"
projects[uw_ofis_eng][download][url] = "https://git.uwaterloo.ca/wcms/uw_ofis_eng.git"
projects[uw_ofis_eng][download][tag] = "7.x-1.12"
projects[uw_ofis_eng][subdir] = ""

; uw_salesforce_block
projects[uw_salesforce_block][type] = "module"
projects[uw_salesforce_block][download][type] = "git"
projects[uw_salesforce_block][download][url] = "https://git.uwaterloo.ca/wcms/uw_salesforce_block.git"
projects[uw_salesforce_block][download][tag] = "7.x-1.4"
projects[uw_salesforce_block][subdir] = ""
