# Waterloo Engineering Research Experts

## Instructions

Once the site.make file is copied you will need to enable the uw_salesforce_block module 
using either drush or the GUI which will then enable the rest of the required modules.